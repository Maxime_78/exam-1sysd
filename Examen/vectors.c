#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void add_vectors(int n, double *t1, double *t2, double *t3) {
	int i;
	for (i = 0; i < n; i++) {
		t3[i] = (t1[i] - t2[i]) * (t1[i] - t2[i]);
	}
}
double norm_vector(int n, double *t) {
    double norm = 0.0;
    for (int i = 0; i < n; i++) {
        norm += t[i];
    }
    return norm;
}
int main() {
	double t1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double t2[] = { 2.71,  2.5,  -1, 3, -7  };
	double t3[5];
	int n = 5;
	int i;
	
	add_vectors(n,t1,t2,t3);
	double norm_diff = norm_vector(n, t3);
	
	printf("T1 : ");
	for (i = 0; i < n; i++) {
		printf("%+.2lf ", t1[i]);
	}
	printf("\nT2 : ");
	for (i = 0; i < n; i++) {
		printf("%+.2lf ", t2[i]);
	}
	printf("\nT3 : ");
	for (i = 0; i < n; i++) {
		printf("%+.2lf ", t3[i]);
	}
	printf("\nLa norme euclidienne pour le tableau 3 est de = %f", norm_diff);
	
	printf("\n");
	
	exit(EXIT_SUCCESS);
}
