1)Quelles sont les étapes nécessaires pour supprimer un élément d'une liste chaînée en connaissant son adresse (un pointeur vers le nœud concerné) ?
Vérifier que le pointeur vers le noeud concerné n'est pas NULL.

Si le noeud à supprimer est la tête de la liste, mettre à jour la tête de la liste pour pointer vers le noeud suivant.

Si le noeud à supprimer n'est pas la tête de la liste, trouver le noeud précédent en parcourant la liste à partir de la tête jusqu'à atteindre le nœud précédent.

Mettre à jour les noeud qui était avant et apres le noeud supprimer pour qu'il puisse continuer a savoir comment aller de l'un a l'autre 

Libérer la mémoire allouée pour le noeud à supprimer à l'aide de la fonction free.

2)Deux fonctions réalisent cette opération dans le fichier linus.c.Quelles sont les différences notables entre ces deux fonctions ?

la difference notable entre c'est deux fonction est que la premiere prend un pointeur pour debuter alors que sur la deuxieme on utilise uniquement des pointeurs ainsi que des doubles pointeurs 

3)Pourquoi la seconde a besoin de renvoyer une valeur et l'autre non ?
Déja ce n'est pas la seconde mais la premiere qui a besoin de renvoyer une valeur car si la premiere valeur venait a etre suprimer il faudrait qu'elle puisse changer la valeur de la suivante comme en nouvelle premiere valeur.
Du coup la deuxieme na pas besoin de retourner une valeur vu qu'elle change directement la valeur de la premiere valeur 

4)Quel est le rôle d'un pointeur vers un pointeur ici ?
le role d'un pointeur vers un pointeur ici sert a pouvoir modifier l'adresse a l'aide d'une autre variable.

5)Pourquoi la seconde reçoit un pointeur vers un pointeur comme argument ?
Car elle doit être capable de modifier la tête de la liste chaînée en cas de suppression du premier élément de la liste.

6)En quelques mots, laquelle est la plus élégante selon vous ?
la seconde car elle utilise des doubles pointeurs qui fait que ca evite les cas partculiers et ca evite aussi d'utiliser des variables intermédiaire 
