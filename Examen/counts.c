#include <stdio.h>

int count_char(char *string, char c) {
    int count = 0;
    while (*string != '\0') {
        if (*string == c) {
            count++;
        }
        string++;
    }
    return count;
}

int main() {
    char str[50];
    char c;
    printf("Entrez une chaîne de caractères : ");
    scanf("%s", str);
    printf("Entrez le caractère à compter : ");
    scanf(" %c", &c);
    int count = count_char(str, c);
    printf("Le caractère '%c' apparaît %d fois dans la chaîne '%s'\n", c, count, str);
    return 0;
}

